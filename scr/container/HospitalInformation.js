import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  ImageBackground,
  TextInput,
  Picker,
  Alert
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import {
  H1,
  H2,
  Toast,
 
} from "native-base";
import { message } from "../constants/string";
import GenericHeader from "../universal/components/GenericHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import { fetchData } from "../action/action";
import { reducerType } from "../constants/reducerType";

class HospitalInformation extends Component {
  constructor(props) {
    super(props);
    const { params } = this.props.navigation.state;
  }
  state = {
    address: "",
    physicianName: "",
    hospitalName: "",
    phoneNo: "",
    email: "",
    cancerTypeId:"",
    stageOfChemotherapy: "Acute Myelogenous Leukemia (AML)",
  };
 


  componentDidUpdate(prevProps, prevState) {
    if(prevProps.patientInformationdata != this.props.patientInformationdata){
      Alert.alert(
        "Success",
        this.props.patientInformationdata.message,
        [
          {
            text: "OK",
            onPress: () =>this.props.navigation.replace('Login')
          }
        ],
        { cancelable: false }
      );
    
  }
  }

  emailValidation = () =>{
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(reg.test(this.state.email)){
      return true
    }else{
      return Toast.show({
        text: "please check your email!",
        buttonText: "Okay",
        position: "top",
        duration: 5000
      })
    }
  }

  _registrationApiCall = () => {
    const { params } = this.props.navigation.state;
    const data = params ? params.data : null;
   
    // if (data && this.emailValidation()) {
      data["username"] = data.emailId;
      data["password"] = data.password;
      data["hospitalName"] = this.state.hospitalName;
      data["cancerTypeId"] = this.state.cancerTypeId;
      data["hospitalPhysician"] = this.state.physicianName;
      data["hospitalEmailid"] = this.state.email;
      data["hosipitalPhoneNumber"] = this.state.phoneNo;
      data["stageOfChemotherapy"] = this.state.stageOfChemotherapy;
    
    this.props.fetchData(reducerType.REGISTRATION, data);
    // }
  }
 

  render() {
    const { navigate } = this.props.navigation;
    {
      this.state.params = this.props.navigation.state.params;
    }
    return (
      <View style={{ backgroundColor: "#fff", marginBottom:100 }}>
       <GenericHeader
            navigation={this.props.navigation}
            headerTitle={"PEEP"}
          />
        <KeyboardAwareScrollView
          style={{ backgroundColor: "#fff" }}
          resetScrollToCoords={{ x: 0, y: 0 }}
          scrollEnabled={true}
        >
         
          <Text style={styles.titleAlign}>{message.REGISTRATION}</Text>
          <Text style={styles.messageText}>{message.HOSPITALINFORMATION}</Text>
          <View style={{ flexDirection: "row" ,width:"95%"}}>
            <View style={styles.circleShape}>
              <TouchableOpacity>
                <Text style={styles.numberAlign}> 1 </Text>
              </TouchableOpacity>
            </View>
            <Text style={{ color: "#61A317" }}> Personal Information </Text>
            <Text>-</Text>
            <View style={styles.emptyCircleShape}>
              <TouchableOpacity>
                <Text style={styles.twonumberAlign}> 2</Text>
              </TouchableOpacity>
            </View>
            <Text style={{ color: "#F69A2E" }}> Cancer Information</Text>
          </View>
          <View style={styles.viewalign}>
          <View style={styles.pickerView}>
            <Picker
              selectedValue={this.state.cancerTypeId}
              mode={"dropdown"}
              style={{ width: "100%" }}
              onValueChange={(itemValue, itemIndex) =>
                this.setState({ cancerTypeId: itemValue })
              }
            >
            <Picker.Item label="Type of cancer" value="Type of cancer" />
              <Picker.Item
                label="Acute Lymphoblastic Leukemia (ALL)"
                value="Acute Lymphoblastic Leukemia (ALL)"
              />
              <Picker.Item
                label="Acute Myelogenous Leukemia (AML)"
                value="Acute Myelogenous Leukemia (AML)"
              />
              <Picker.Item label="Anal" value="Anal" />
              <Picker.Item label="Appendiceal" value="Appendiceal" />
              <Picker.Item label="Bladder" value="Bladder" />
              <Picker.Item label="Brain" value="Brain" />
              <Picker.Item label="Breast" value="Breast" />
              <Picker.Item label="Cervical" value="Cervical" />
              <Picker.Item
                label="Chronic Lymphocytic Leukemia (CLL)"
                value="Chronic Lymphocytic Leukemia (CLL)"
              />
              <Picker.Item
                label="Chronic Myelogenous Leukemia (CML)"
                value="Chronic Myelogenous Leukemia (CML)"
              />
              <Picker.Item label="Colon" value="Colon" />
              <Picker.Item label="Endometrial" value="Endometrial" />
              <Picker.Item label="Fallopian tube" value="Fallopian tube" />
              <Picker.Item label="Gall bladder" value="Gall bladder" />
              <Picker.Item label="Head and Neck" value="Head and Neck" />
              <Picker.Item label="Esophageal" value="Esophageal" />
              <Picker.Item
                label="Hodgkin’s Lymphoma (Hodgkin’s Disease)"
                value="Hodgkin’s Lymphoma (Hodgkin’s Disease)"
              />
              <Picker.Item label="Kidney" value="Kidney" />
              <Picker.Item label="Liver" value="Liver" />
              <Picker.Item label="Lung" value="Lung" />
              <Picker.Item label="Mesolthelioma" value="Mesolthelioma" />
              <Picker.Item label="Multiple Myeloma" value="Multiple Myeloma" />
              <Picker.Item
                label="Non-Hodgkin’s Lymphoma"
                value="Non-Hodgkin’s Lymphoma"
              />
              <Picker.Item label="Ovarian" value="Ovarian" />
              <Picker.Item label="Pancreatic" value="Pancreatic" />
              <Picker.Item
                label="Pediatric - Acute Lymphoblastic Leukemia (ALL)"
                value="Pediatric - Acute Lymphoblastic Leukemia (ALL)"
              />
              <Picker.Item
                label="Pediatric - Acute Myelogenous Leukemia (AML)"
                value="Pediatric - Acute Myelogenous Leukemia (AML)"
              />
              <Picker.Item
                label="Pediatric - Brain Tumors"
                value="Pediatric - Brain Tumors"
              />
              <Picker.Item
                label="Pediatric - Carcinomas"
                value="Pediatric - Carcinomas"
              />
              <Picker.Item
                label="Pediatric - Chronic Lymphocytic Leukemia (CLL)"
                value="Pediatric - Chronic Lymphocytic Leukemia (CLL)"
              />
              <Picker.Item
                label="Pediatric – Hepatoblastoma"
                value="Pediatric – Hepatoblastoma"
              />
              <Picker.Item
                label="Pediatric - Hodgkin’s Lymphoma (Hodgkin’s Disease)"
                value="Pediatric - Hodgkin’s Lymphoma (Hodgkin’s Disease)"
              />
              <Picker.Item
                label="Pediatric – Neuroblastoma"
                value="Pediatric – Neuroblastoma"
              />
              <Picker.Item
                label="Pediatric - Non-Hodgkin’s Lymphoma"
                value="Pediatric - Non-Hodgkin’s Lymphoma"
              />
              <Picker.Item
                label="Pediatric – Retinoblastoma"
                value="Pediatric – Retinoblastoma"
              />
              <Picker.Item
                label="Pediatric – Sarcomas"
                value="Pediatric – Sarcomas"
              />
              <Picker.Item
                label="Pediatric - Wilms’ Tumor"
                value="Pediatric - Wilms’ Tumor"
              />
              <Picker.Item label="Penile" value="Penile" />
              <Picker.Item label="Prostate" value="Prostate" />
              <Picker.Item label="Rectal" value="Rectal" />
              <Picker.Item label="Sarcoma" value="Sarcoma" />
              <Picker.Item label="Skin (melanoma)" value="Skin (melanoma)" />
              <Picker.Item
                label="Stomach (gastric)"
                value="Stomach (gastric)"
              />
              <Picker.Item label="Testicular" value="Testicular" />
              <Picker.Item label="Thyroid/Thymus" value="Thyroid/Thymus" />
              <Picker.Item label="Vaginal/vulvar" value="Vaginal/vulvar" />
              <Picker.Item label="Other" value="Other" />
              <Picker.Item label="I don’t know" value="I don’t know" />
            </Picker>
          </View>

           <Text style={styles.subRadioTitle}>Have you received chemotherapy treatments in the past?</Text>
            <RadioGroup
              onSelect={(index, value) =>
                this.setState({ stageOfChemotherapy: value })
              }
              style={{ marginStart: 20 }}
            >
              {this.radio("No", "No")}
              {this.radio("Yes", "Yes")}
              {this.radio("I dont know", "I dont know")} 
            </RadioGroup>
          </View>

          <TouchableOpacity
            onPress={this._registrationApiCall}
            style={styles.button}
          >
            <Text style={styles.buttonText}>{message.REGISTRATION}</Text>
          </TouchableOpacity>
        </KeyboardAwareScrollView>
      </View>
    );
  }

  radio = (value, text) => {
    return (
      <RadioButton value={value} style={styles.radioItem}>
        <Text>{text}</Text>
      </RadioButton>
    );
  };
}
const styles = {
  viewalign: {
    marginStart: 15,
    marginTop: 15,
    marginBottom: 15
  },
  titleAlign: {
    marginStart: 15,
    marginTop: 15,
    marginBottom: 15,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 25
  },
  messageText: {
    fontSize: 17,
    marginStart: 15,
    marginBottom: 25,
    marginEnd: 15
  },
  circleShape: {
    marginStart: 15,
    width: 20,
    height: 20,
    alignItems: "center",
    borderRadius: 20 / 2,
    backgroundColor: "#61A317"
  },
  emptyCircleShape: {
    width: 20,
    height: 20,
    marginStart: 15,
    alignItems: "center",
    borderRadius: 20 / 2,
    backgroundColor: "#F69A2E"
  },
  twonumberAlign: {
    fontSize: 15,
    color: "#E0E0E0"
  },
  numberAlign: {
    fontSize: 15,
    color: "#E0DEDA"
  },

  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },
  textInput: {
    padding: 5,
    marginStart: 10,
    fontSize: 15,
    marginEnd: 10,
    height:40,
    borderBottomColor:'gray',
    borderBottomWidth:1
  },
  subRadioTitle: {
    color: "#5c5c3d",
    marginBottom: 5,
    marginTop: 5,
    padding: 10,
    marginStart: 15,
    fontSize: 15,
    marginEnd: 15
  },
  radioItem: {
    margin:10,
    marginStart: 15,
    borderRadius: 10,
    alignItems: "center",
    height: 40,
    width: "80%",
    borderWidth: 1,
    borderColor: "#5c5c3d"
  },
  radio: {
    flexDirection: "row"
  }
};

const mapStateToProps = state => {
  return {
    patientInformationdata: state.GetModuleType.patientInformationdata
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchData: fetchData
    },
    dispatch
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(
  HospitalInformation
);
