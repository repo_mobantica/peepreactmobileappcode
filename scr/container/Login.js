import React, { Component } from "react";
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Alert,
  AsyncStorage,
  Image
} from "react-native";
import { Toast } from "native-base";
import GenericHeader from "../universal/components/GenericHeader";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { message } from "../constants/string";
import { fetchData, LoginclearData } from "../action/action";
import { reducerType } from "../constants/reducerType";
import Spinner from "../universal/components/Spinner";

const ContainerWithSpinner = Spinner(View);

class Login extends Component {
  state = {
    userName: "digvijay@mobantica.com",
    password: "qwerty"
  };

  async componentDidUpdate(prevProps, prevState) {
    if (prevProps.loginData != this.props.loginData) {
      if (this.props.loginData == 1) {
        await AsyncStorage.setItem(
          message.SAVE_LOGIN_DATA,
          JSON.stringify(this.props.patientinfo)
        );
        this.props.navigation.replace("Dashboard");
      } else if (this.props.loginData) {
        Alert.alert(
          "Error",
          this.props.message,
          [
            {
              text: "OK",
              onPress: () => this.props.clearData()
            }
          ],
          { cancelable: false }
        );
      }
    }
  }
  _loginApiCall = () => {
    if (this.state.userName == "" || this.state.password == "") {
      return Toast.show({
        text: "email Id or password is blank",
        buttonText: "Okay",
        position: "top",
        duration: 5000
      });
    } else if (this.emailValidation()) {
      const body = {
        username: this.state.userName,
        password: this.state.password
      };

      this.props.fetchData(reducerType.GET_LOGIN_CALL, body);
    }
  };

  emailValidation = () => {
    const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (reg.test(this.state.userName)) {
      return true;
    } else {
      return Toast.show({
        text: "Wrong Email Please Check!",
        buttonText: "Okay",
        position: "top",
        duration: 5000
      });
    }
  };

  goBack() {
    const { replace } = this.props.navigation;
    replace("Registration");
  }

  render() {
    const msg = this.props.loginData && this.props.loginData.message;

    return (
      <ContainerWithSpinner
        isLoading={this.props.isLoading}
        style={{ top: 80 ,zIndex: 0
        }}
      >
        <Image
          style={{ alignSelf: "center", width: 80, height: 50, margin: 10 }}
          source={require("../asset/app_logo.png")}
        />
        <Text style={styles.messageText}> {message.LOGIN} </Text>
        <TextInput
          style={styles.textInput}
          returnKeyType={"next"}
          underlineColorAndroid="transparent"
          autoCapitalize={"none"}
          keyboardType={"email-address"}
          onSubmitEditing={() => {
            this.password.focus();
          }}
          placeholder={"Email Id"}
          onChangeText={userName => this.setState({ userName })}
          value={this.state.userName}
        />
        <TextInput
          style={styles.textInput}
          ref={input => {
            this.password = input;
          }}
          secureTextEntry={true}
          underlineColorAndroid="transparent"
          placeholder={"Password"}
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
        />
        <TouchableOpacity
          onPress={() => this.props.navigation.navigate("ForgotPassword")}
        >
          <Text style={styles.titleAlign}>Forgot Password ?</Text>
        </TouchableOpacity>

        <TouchableOpacity
          onPress={() => this._loginApiCall()}
          style={styles.button}
        >
          <Text style={styles.buttonText}>{message.LOGIN}</Text>
        </TouchableOpacity>
        <Image style={{ flex: 1 }} source={require("../asset/register.jpeg")} />
        <TouchableOpacity onPress={() => this.goBack()}>
          <Text style={styles.messageText}> Back </Text>
        </TouchableOpacity>
      </ContainerWithSpinner>
    );
  }
}

const styles = {
  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },

  textInput: {
    padding: 10,
    marginStart: 15,
    fontSize: 15,
    marginEnd: 15,
    borderBottomWidth: 1,
    borderBottomColor: "gray"
  },

  messageText: {
    textAlign: "center",
    fontSize: 20,
    marginBottom: 25,
    color: "#00cccc",
    fontWeight: "bold"
  },
  titleAlign: {
    textAlign: "right",
    marginEnd: 15,
    marginTop: 10,
    fontSize: 14
  }
};

const mapStateToProps = state => {
  return {
    loginData: state.GetModuleType.loginData,
    message: state.GetModuleType.messageLogin,
    patientinfo: state.GetModuleType.patientinfo,
    isLoading: state.GetModuleType.isLoading
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchData: fetchData,
      clearData: LoginclearData
    },
    dispatch
  );
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
