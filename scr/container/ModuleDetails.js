import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image, 
  FlatList
} from "react-native";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { H1, H2, H3, Item, Label, Input, Form, Content } from "native-base";
import { message } from "../constants/string";
import GenericHeader from "../universal/components/GenericHeader";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import * as Progress from "react-native-progress";
import { fetchData } from "../action/action";
import { reducerType } from "../constants/reducerType";
import Icon from "react-native-vector-icons/FontAwesome";
import Spinner from "../universal/components/Spinner";
const ContainerWithSpinner = Spinner(View);

class ModuleDetails extends Component {
  constructor(props){
    super(props)
    //this.moduleAPICall = this.moduleAPICall.bind(this)
  }
  componentDidMount() {
     //this.moduleAPICall()
     this._didBlurAPICall =  this.props.navigation.addListener("didFocus", this.moduleAPICall);
  }
  
  componentWillUnmount(){
    // Remove the listener when you are done
    this._didBlurAPICall.remove();
  }

  moduleAPICall = () =>{
    const { params } = this.props.navigation.state;
    const body = {
      username: this.props.emailId,
      password: this.props.password,
      pageNo: "1",
      moduleId: params ? params.moduleId : null
    };
    this.props.fetchData(reducerType.GET_ACTIVITY_LIST, body);
  }
 

  _keyExtractor = (item, index) => item.activityId
  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;
    const titleName = params && params.moduleName;
    const activityCount = this.props.activityList
      ? this.props.activityList.totalactivites
      : 0;
      const moduleWisePer= this.props.activityList? this.props.activityList.moduleWisePer: null
const totalQuestionCount = this.props.activityList? this.props.activityList.totalquestionCount:null

    // const sum =0
    // const totalQuestion = this.props.activityList.map((data)=>{return( questionCount + sum)})


    return (
      <ContainerWithSpinner
        style={{ backgroundColor: "#fff", flex: 1 }}
        isLoading={this.props.isLoading}
      >
        <GenericHeader
          navigation={this.props.navigation}
          headerTitle={"Activity Details"}
        />
        <KeyboardAwareScrollView
          style={{ backgroundColor: "#fff" }}
          //scrollEnabled={true}
        >
        <View
          style={{
            backgroundColor: "#00cccc",
            height: 200,
            flexDirection:'row',
            padding:10
            //alignItems: "center"
          }}
        >
        <Image
            style={{margin:10, width:50, height:40}}
            source={
              require("../asset/video_white.png")
            }
          />
 
          <View>
          <Text style={[styles.titleAlign, { color: "#fff", fontSize: 20 }]}>
            {titleName}
          </Text>
          <Text style={{ fontSize: 12, margin: 5, color: "#fff" }}>
            {activityCount} Activity {"  |  "}{totalQuestionCount} Question
          </Text>
          
          <Progress.Bar
                        style={{ marginTop: 10,marginEnd:10  }}
                        color={"#ffbf00"}
                        unfilledColor= {"fff"}
                        progress={moduleWisePer/100}
                        width={200}
                      />

          </View>
        </View>
        {this.props.activityList && (
          <FlatList
            style={{ zIndex:1, top: -50}}
            data={this.props.activityList.activities}
            keyExtractor={this._keyExtractor}
            renderItem={({ item, index }) => (
              <View style={styles.cardView}>
                <Text style={{ fontSize: 15 }}>
                  Activity - {index + 1}
                </Text>
                <Text style={{ fontSize: 17, color: "#000" , marginBottom:10,marginTop:10}}>
                  {item.activityName}
                </Text>
                <Text style={{ fontSize: 16, color: "#00cccc" }}>
                <Image
             style = {{height:11, width:20}}
             source = {require('../asset/eye.png')}
             />
                 
                      {"  "}{item.watchVideoCount} video watched
                </Text>
                <Text style={{ fontSize: 15, color: "#00cccc" }}>
                <Image
             style = {{height:11, width:20}}
             source = {require('../asset/eye.png')}
             /> 
                 {"  "}{item.watchQuestionCount} Question Answered
                </Text>
                <TouchableOpacity
                  onPress={() =>
                    navigate("Activity", { activityId: item.activityId, item: item, })
                  }
                  style={styles.button}
                >
                  <Text style={styles.buttonText}>See the Activity</Text>
                </TouchableOpacity>
              </View>
            )}
          />
        )}
        </KeyboardAwareScrollView>
      </ContainerWithSpinner>
    );
  }
}

const styles = {
  pickerView: {
    flexDirection: "row",
    marginStart: 15,
    marginTop: 5,
    justifyContent: "space-between"
  },
  cardView: {
    margin: 20,
    padding: 20,
    borderRadius: 10,
    justifyContent: "space-around",
    borderColor: "#00cccc",
    borderWidth: 1,
    marginTop: 10,
    backgroundColor: "#fff"
  },
  button: {
    alignItems: "center",
    backgroundColor: "#00cccc",
    borderRadius: 20,
    margin: 20,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },

  textInput: {
    padding: 10,
    marginStart: 15,
    fontSize: 15,
    marginEnd: 15
  },
  titleAlign: {
    marginTop: 15,
    marginBottom: 15,
    color: "#00cccc",
    fontSize: 25
  },
  subTitle: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginBottom: 5
  },
  messageText: {
    fontSize: 17,
    marginStart: 15,
    marginBottom: 25,
    marginEnd: 15
  }
};

const mapStateToProps = state => {
  return {
    // activities: state.GetModuleType.activities,
    emailId: state.GetModuleType.patientinfo.emailId,
    password: state.GetModuleType.patientinfo.password,
    activityList: state.GetModuleType.activities, 
    // isLoading: state.GetModuleType.isLoading
    isLoading: false
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchData: fetchData
    },
    dispatch
  );
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ModuleDetails);
