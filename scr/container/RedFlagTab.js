import React, { Component } from "react";
import { View, Image, Text } from "react-native";
import { Tab, Tabs, TabHeading, Icon } from "native-base";
import GenericHeader from '../universal/components/GenericHeader';
import AddRedFlag from './AddRedFlag'
import RedFlagReport from './RedFlagReport'


class RedFlagTab extends Component {
  header(titleName) {
    return (
      <TabHeading style={{backgroundColor: '#00cccc' }}>
       <Text style = {{fontSize:15, color:'#000'}}>{titleName}</Text>
      </TabHeading>
    );
  }
  componentDidMount(){
  //  const {params}= this.props.navigation.state
  //  this.saveKey(params.groupId,params.memberId)
}
 
  tabView(Name, Comp) {
    // const {params}= this.props.navigation.state
    return (
      <Tab heading={this.header( Name)}>
        <Comp 
        />
      </Tab>
    );
  }
  
  state = {
    headerTitle: "Flag"
  }

  render() {
    const {params}= this.props.navigation.state
    return (
      <View style ={{flex:1}}>
      <GenericHeader
      navigation={this.props.navigation}
      headerTitle={this.state.headerTitle} />
        <Tabs
          tabBarPosition={'top'}
          tabBarUnderlineStyle={{
            backgroundColor: "#ffbf00"
          }}
        >
          {this.tabView("Add Red Flag Symptoms", AddRedFlag)}
          {this.tabView("See Previous Log Records", RedFlagReport)}
       
        </Tabs>
        </View>
    );
  }
}


export default RedFlagTab;
