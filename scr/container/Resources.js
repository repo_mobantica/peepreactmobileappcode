import React, { Component } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  Linking
} from "react-native";
import { Toast } from 'native-base'
import GenericHeader from "../universal/components/GenericHeader";
import { message } from "../constants/string";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import ArrayData from "../utils/json.json";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import { fetchData } from "../action/action";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { reducerType } from "../constants/reducerType";
import { postApiCallWithPromise } from "../utils/PromiseApiCall";
import { Url } from "../utils/constant/Url";
import Icon from "react-native-vector-icons/FontAwesome";
import Spinner from "../universal/components/Spinner";
const ContainerWithSpinner = Spinner(View);

class Resources extends Component {
  state = {
    selectedValue: "",
    backColor: "#fff",
    disablrButoon: false,
    textColor: "#3399ff",
    radioValueData: [],
    uniqueData: []
  };

  componentDidMount() {
    this.getQuestionAnswerFromApi()
  }

  getQuestionAnswerFromApi = () => {
    const { params } = this.props.navigation.state;
    const body = {
      username: this.props.emailId,
      password: this.props.password,
    };
    this.props.fetchData(reducerType.GET_VIDEO_RESOURSES, body);
    this.props.fetchData(reducerType.GET_ARTICLE_RESOURSES, body);

  }
  _keyExtractor = (item, index) => item.videoId;

  render() {
    const { navigate } = this.props.navigation;
    const { params } = this.props.navigation.state;


    return (
      <ContainerWithSpinner
        style={{ backgroundColor: "#fff", flex: 1 }}
        isLoading={this.props.isLoading}
      >

        <GenericHeader
          navigation={this.props.navigation}
          headerTitle={"Resources"}
        />
        <KeyboardAwareScrollView
          style={{ backgroundColor: "#fff" }}
          scrollEnabled={true}
        >
          <Text style={styles.textDetails}>
            Videos
              </Text>
          {this.props.videoResources ? (
            <View>
              <FlatList
                data={this.props.videoResources.responseResource}
                keyExtractor={this._keyExtractor}
                renderItem={({ item }) => (
                  <View>
                    <TouchableOpacity
                      style={styles.touchableView}
                      onPress={() =>
                        navigate("PlayVideo", { videoLink: item.videoUrl })
                      }
                    >
                      <Image
                        style={styles.iconView}
                        source={require('../asset/video_green.png')}
                      />

                      <Text style={styles.subTitle}>{item.resourceName}</Text>
                    </TouchableOpacity>
                  </View>
                )}
              />

            </View>
          ) : null}

          <Text style={styles.textDetails}>
            Articles
              </Text>

          {this.props.articleResources ? (
            <View>
              <FlatList
                data={this.props.articleResources.articles}
                keyExtractor={this._keyExtractor}
                renderItem={({ item }) => (
                  <View>
                    <TouchableOpacity
                      style={styles.touchableView}
                      onPress={() => { Linking.openURL(item.articleUrl) }}
                    >
                      <Text style={[styles.subTitle, {marginBottom:5}]}>{item.articleUrl}</Text>
                    </TouchableOpacity>
                  </View>
                )}
              />

            </View>
          ) : null}
        </KeyboardAwareScrollView>
      </ContainerWithSpinner>
    );
  }
}
const styles = {
  contentContainer: {
    paddingVertical: 20
  },
  subRadioTitle: {
    color: "#5c5c3d",
    marginBottom: 5,
    padding: 10,
    marginStart: 15,
    fontSize: 15,
    marginEnd: 15
  },
  radioItem: {
    margin: 10,
    marginStart: 15,
    borderRadius: 10,
    alignSelf: "center",
    height: 40,
    width: "70%",
    borderWidth: 1,
    borderColor: "#5c5c3d"
  },
  titleAlign: {
    marginStart: 15,
    marginTop: 5,
    fontWeight: "bold",
    color: "#fff", fontSize: 18
  },
  subTitle: {
    fontSize: 16,
    marginStart: 15,
    color: "#5c5c3d",
    marginTop: 5,
  },
  touchableView: {
    flexDirection: "row",
    alignItems: "center",
    width: '100%',
    marginLeft: 10,

  },
  iconView: {
    height: 40,
    width: "10%",
    marginStart: 10,
    marginTop: 10,
    marginLeft: 10
  },
  cardTitle: {
    fontSize: 17,
    textAlign: "left",
    marginStart: 5,
    width: "60%"
  },
  articlesTitle: {
    fontSize: 17,
    textAlign: "left",
    marginStart: 15,
    width: "90%",
    marginBottom: 5,
    marginTop: 5
  },
  cardView: {
    margin: 5,
    borderRadius: 10,
    alignItems: "flex-start",
    backgroundColor: "#fff"
  },
  button: {
    borderColor: "#cca300",
    borderRadius: 10,
    marginTop: 20,
    marginLeft: 15,
    backgroundColor: "#cca300",
    marginBottom: 10,
    width: '45%',
    paddingLeft: 5,
    paddingRight: 5,
    borderWidth: 1
  },
  textDetails: {
    color: "#5c5c3d",
    fontSize: 16,
    textAlign: "left",
    marginLeft: 20,
    marginTop: 10,
    alignItems: 'center'
  },
  buttonText: {
    fontSize: 14,
    alignSelf: 'center',
    color: "#3399ff",
    marginLeft: 30,
  }
};

const mapStateToProps = state => {
  return {
    emailId: state.GetModuleType.patientinfo.emailId,
    password: state.GetModuleType.patientinfo.password,
    isLoading: state.GetModuleType.isLoading,
    videoResources: state.GetModuleType.videoResources,
    articleResources: state.GetModuleType.articleResources,
  };
};

const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchData: fetchData
    },
    dispatch
  );
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Resources);
