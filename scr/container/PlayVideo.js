import React, { Component } from "react";
import { View, Text, WebView, Image, StyleSheet } from "react-native";
import GenericHeader from "../universal/components/GenericHeader";
import { message } from "../constants/string";
import Video from "react-native-video";

//import {Video} from"react-native-video";
class PlayVideo extends Component {
 
  render() {
    const { params } = this.props.navigation.state
    const VideoLink = params? params.videoLink:'http://d23dyxeqlo5psv.cloudfront.net/big_buck_bunny.mp4'

    return (
      <View style={styles.container}>
         <GenericHeader
            navigation={this.props.navigation}
            headerTitle={"Video Player"}
          />
      <WebView
                    style={ styles.WebViewContainer }
                    javaScriptEnabled={true}
                    domStorageEnabled={true}
                    source={{uri: VideoLink }}
            />
      </View>
    );
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  tabPlay:{
    fontSize:18,
    textAlign:'center',
    color:'#fff',
    margin:10
  },
  fullScreen: {
    position: 'absolute',
    top: 50,
    left: 0,
    bottom: 50,
    right: 0,
  },
  controls: {
    backgroundColor: 'transparent',
    borderRadius: 5,
    position: 'absolute',
    bottom: 20,
    left: 20,
    right: 20,
  },
  progress: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 3,
    overflow: 'hidden',
  },
  innerProgressCompleted: {
    height: 20,
    backgroundColor: '#cccccc',
  },
  innerProgressRemaining: {
    height: 20,
    backgroundColor: '#2C2C2C',
  },
  generalControls: {
    flex: 1,
    flexDirection: 'row',
    borderRadius: 4,
    overflow: 'hidden',
    paddingBottom: 10,
  },
  rateControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  volumeControl: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  resizeModeControl: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  controlOption: {
    alignSelf: 'center',
    fontSize: 11,
    color: 'white',
    paddingLeft: 2,
    paddingRight: 2,
    lineHeight: 12,
  },
});
export default PlayVideo;
