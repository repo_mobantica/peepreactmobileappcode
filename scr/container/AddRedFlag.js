import React, { Component } from "react";
import { View, Text, TouchableOpacity, FlatList, Alert } from "react-native";
import { message } from "../constants/string";
import ArrayData from "../utils/json.json";
import { RadioGroup, RadioButton } from "react-native-flexi-radio-button";
import { connect } from "react-redux";
import { reducerType } from '../constants/reducerType'
import { fetchData } from '../action/action'
import { bindActionCreators } from "redux";
import { Toast } from 'native-base'
import { Url } from "../utils/constant/Url";
import { postApiCallWithPromise } from "../utils/PromiseApiCall";
import Spinner from "../universal/components/Spinner";

const ContainerWithSpinner = Spinner(View);

class AddRedFlag extends Component {
  state = {
    uniqueData: [],
    radioValueData: [],
    isLoading:false
  };
  componentDidMount() { 
    const body = {
      username: this.props.emailId,
      password: this.props.password, 
    };
    this.props.fetchData(reducerType.RED_FLAG_OPTION, body);
  }

  render() {
    const { resonseSymptoms } = this.props.redFlagQuestion ? this.props.redFlagQuestion : { }
    return (
      <ContainerWithSpinner
      isLoading = {this.props.isLoading? this.props.isLoading: this.state.isLoading }
       style={{ backgroundColor: "#E7E7E7", flex: 1 }}>
        <Text style={styles.titleAlign}>{message.CHECK_FOR_RED_FLAG}</Text>
        <FlatList
          data={resonseSymptoms}
          renderItem={({ item , index}) => (
            <View>
              <Text style={styles.subRadioTitle}> Q{index+1}.{" "}{item.redFlagQuestionName}</Text>
             
              <RadioGroup
                      onSelect={(index, value) => this.setRadioValue(value, item)}
                      selectedIndex = {item.wsRedflagOption.findIndex(x => x.redFlagQuestionId == item.correctOptionId)}
                    >
               {
                item.wsRedflagOption.map((data,index)=>{return(
               <RadioButton value={data.redFlagOptionId} style={styles.radioItem}>
                <Text>{data.redFlagOptionName}</Text>
              </RadioButton>
         
             
                 )})  }
                  </RadioGroup>
            </View>
          )}
        />
       
        <TouchableOpacity
          onPress={() => this.SubmitAPICall()}
          style={styles.button}
        >
          <Text style={styles.buttonText}>Submit</Text>
        </TouchableOpacity>
      </ContainerWithSpinner>
    );
  }

  SubmitAPICall() {
    //const { goBack } = this.props.navigation
    if (this.state.radioValueData && this.state.radioValueData.length > 0) {
      const body = {
        addRedFlagSymtoms: this.state.radioValueData
      };
      this.setState({isLoading:true})
      postApiCallWithPromise(Url.addRedFlagSymptoms, body)
        .then(response => {
          this.setState({isLoading:false})
          Alert.alert(
            "Success",
            response.data.message,
            [{ text: "OK", //onPress: () => goBack() 
          }],
            { cancelable: false }
          );
        })
        .catch((error)=> {
          this.setState({isLoading:false})
          reject(error);
        });
    } else {
      Toast.show({
        text: "Please Complete the Question Answer First",
        buttonText: "Okay",
        position: "top",
        duration: 3000
      })
    }
  }
   findUnique(arr, predicate) {
    var found = {};
    arr.forEach(d => {
      found[predicate(d)] = d;
    });
    return Object.keys(found).map(key => found[key]); 
  }

  setRadioValue(optionId, data) {
    //const { params } = this.props.navigation.state;
    
    const array = {
      redFlagQuestionId: data.redFlagQuestionId,
      correctOptionId: '',
      userId: this.props.UserId, 
      redFlagAnswerId: optionId
    };
    //if (this.state.radioValueData.length>0) {
      this.state.radioValueData.push(array);
       this.state.radioValueData = this.findUnique(this.state.radioValueData, d => d.redFlagQuestionId);
       console.log("List",this.state.radioValueData)
  }
}

styles = {
  titleAlign: {
    margin: 10,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 20
  },
  button: {
    alignItems: "center",
    backgroundColor: "#cca300",
    borderRadius: 10,
    margin:10,
    paddingLeft: 20,
    paddingRight: 20
  },
  buttonText: {
    fontSize: 18,
    color: "white",
    margin: 12
  },
  subRadioTitle: {
    color: "#000",
    marginBottom: 5,
    padding: 10,
    marginStart: 10,
    fontSize: 15,
    marginEnd: 15
  },
  radioItem: {
    margin: 10,
    marginStart: 15,
    borderRadius: 10,
    alignItems: "center",
    height: 40,
    width: "80%",
    borderWidth: 1,
    borderColor: "#5c5c3d"
  },
};
const mapStateToProps = state => {
  return {
    loginData: state.GetModuleType.patientinfo,
    emailId: state.GetModuleType.patientinfo.emailId,
    password: state.GetModuleType.patientinfo.password,
    UserId: state.GetModuleType.patientinfo.id,
    redFlagQuestion: state.GetModuleType.redFlagQuestion,
    isLoading: state.GetModuleType.isLoading
  };
};
const mapDispatchToProps = dispatch => {
  return bindActionCreators(
    {
      fetchData: fetchData
    },
    dispatch
  );
};



export default connect(mapStateToProps, mapDispatchToProps)(AddRedFlag);
