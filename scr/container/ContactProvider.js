import React, { Component } from "react";
import { View, Text } from "react-native";
import GenericHeader from "../universal/components/GenericHeader";
import { message } from "../constants/string";
import { connect } from "react-redux";

class ContactProvider extends Component {
 
  render() {
    const loginData = this.props.loginData
    return (
      loginData && (
        <View>
          <GenericHeader
            navigation={this.props.navigation}
            headerTitle={"Contact Providers"}
          />

          <Text style={styles.titleAlign}>{message.CONTACTDETAILS}</Text>
          <Text style={styles.subTitle}>{message.HOSPITAL}</Text>
          <Text style={styles.hospitalDetails}>{loginData.hospitalName}</Text>

          <Text style={styles.subtitleContact}>{message.CONTACTPERSON}</Text>
          <Text style={styles.hospitalDetails}>
            {loginData.hospitalPhysician}{" "}
          </Text>
          <Text style={styles.hospitalDetails}>
            {loginData.hosipitalPhoneNumber}{" "}
          </Text>
          <Text style={styles.hospitalDetails}>
            {loginData.hospitalEmailid}
          </Text>
        </View>
      )
    );
  }
}
const styles = {
  titleAlign: {
    marginStart: 15,
    marginTop: 15,
    marginBottom: 15,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 25
  },
  subTitle: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginBottom: 5
  },
  hospitalDetails: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    marginBottom: 5,
    
  },

  subtitleContact: {
    fontSize: 20,
    marginStart: 15,
    color: "#5c5c3d",
    fontWeight: "bold",
    marginTop: 25,
    marginBottom: 5
  }
};
const mapStateToProps = state => {
  return {
    loginData: state.GetModuleType.patientinfo
  };
};

export default connect(mapStateToProps, null)(ContactProvider);
