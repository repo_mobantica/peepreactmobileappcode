import React, { Component } from 'react';
import promise from 'promise';
import axios from 'axios';
import { View, Text, Alert } from 'react-native';
import CustomNetInfo from "./CustomNetInfo";

// we always get same instance of CustomNetInfo Class
const netInfo = CustomNetInfo;


// We don't want to show more than one similar internet error alerts
let isNoInternetAlertShowing = false;
const noInternetAlert = () => {
  if (!isNoInternetAlertShowing) {
    isNoInternetAlertShowing = true;
    return Alert.alert(
        'Alert',
        'no internet connection',
        [
        {
          text: "OK",
          onPress: () => {
            isNoInternetAlertShowing = false;
          }
        }
      ],
      { cancelable: false }
    );
  }
};


export const getApiCallWithPromise = (url) => {
//  if (!netInfo.isConnected) {
//      return noInternetAlert();
//   }   
    return new promise(function (resolve, reject) {
        axios.get(`${url}`,
                 {
                    headers: {
                        "Content-Type": "application/json",
                        
                      }
                 }
                 ).then(response => {
                         resolve(response);
                 })
                 .catch(error => {
                  //  Alert.alert('Error '+error.response.data);
                    reject(error);
              
                 })          
            })
    }

    export const postApiCallWithPromise = (url, body ) => {
        // if (!netInfo.isConnected) {
        //     return noInternetAlert();
        //  }   
        return new promise(function (resolve, reject) {
            axios.post(`${url}`,body,
                     {
                        headers: {
                            "Content-Type": "application/json",
                        }
                     }
                     ).then(response => {
                             resolve(response);
                     })
                     .catch(error => {
                        //Alert.alert(''+error.response.data);
                        resolve(error);
                    
                     })          
                })
        }