import {
    StackNavigator,
  } from 'react-navigation';

  console.disableYellowBox = true
  import Registration from '../container/Registration' 
  import RegistrationForm from '../container/RegistrationForm'
  import Dashboard from '../container/Dashboard'
  import ModuleDetails from '../container/ModuleDetails'
  import ContactProvider from '../container/ContactProvider'
  import Login from '../container/Login'
  import Resources from "../container/Resources"
  import PlayVideo from '../container/PlayVideo'
  import HospitalInformation from "../container/HospitalInformation"
  import RedFlagTab from '../container/RedFlagTab'
  import Settings from '../container/Settings'
  import Activity from '../container/Activity'
  import FeedBack from '../container/Feedback'
  import ChangePassword from '../container/ChangePassword'
  import UpdateProfile from '../container/UpdateProfile'
  import ForgotPassword from '../container/ForgotPassword'

  const Navigation = StackNavigator({
    
    Registration: { screen: Registration },  
    Activity:{ screen: Activity},  
    Login:{ screen: Login}, 
    Settings:{ screen :Settings},
    RegistrationForm:{ screen : RegistrationForm},    
    RedFlagTab:{ screen : RedFlagTab},
    Dashboard : { screen: Dashboard},
    ModuleDetails:{screen:ModuleDetails},
    ContactProvider:{screen : ContactProvider},
    Resources:{screen:Resources},
    HospitalInformation:{screen:HospitalInformation},
    PlayVideo:{screen: PlayVideo},
    FeedBack:{ screen: FeedBack},
    ChangePassword:{screen : ChangePassword},
    UpdateProfile:{screen: UpdateProfile},
    ForgotPassword:{screen: ForgotPassword}
  },
  {
    // see next line
    mode: "none",
    headerMode: "none",
    navigationOptions: {
      gesturesEnabled: false
    }});

  export default Navigation