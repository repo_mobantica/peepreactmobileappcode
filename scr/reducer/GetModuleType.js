import { reducerType } from '../constants/reducerType'

function GetModuleType(state = [], action) {
  switch (action.type) {
    case reducerType.GET_LOGIN_CALL:
      return {
        ...state,
        loginData: action.payload.status,
        patientinfo: action.payload.patientinfo,
        messageLogin: action.payload.message,
      }
      break;
    case reducerType.ADD_LOCAL_DATA:
      return {
        ...state,
        patientinfo: action.id
      }
    case reducerType.LOGOUT:
      return {
        ...state,
        loginData: undefined

      }
      break;
    case reducerType.GET_MODULE_TYPE:
      return {
        ...state,
        data: action.payload.data,
        moduleType: action.payload.moduleType,
        progressDataHomePage: action.payload.final_dash_per
      }
      break;
    case reducerType.IS_LOADING:
      return {
        ...state,
        isLoading: action.isLoading
      }
      break;
    case reducerType.GET_ACTIVITY_LIST:
      return {
        ...state,
        activities: action.payload
      }
      break;
    case reducerType.REGISTRATION:
      return {
        ...state,
        patientInformationdata: action.payload
      }
      break;
    case reducerType.GET_RESOURCE:
      return {
        ...state,
        responseResource: action.payload.responseResource
      }
    case reducerType.GET_QUESTION_AND_VIDEO:
      return {
        ...state,
        questionAndVideo: action.payload
      }
    case reducerType.GET_RED_FLAG_REPORT:
      return {
        ...state,
        getRedFlagReport: action.payload
      }
    case reducerType.RED_FLAG_OPTION:
      return {
        ...state,
        redFlagQuestion: action.payload
      }
    case reducerType.UPDATE_PASSWORD:
      return {
        ...state,
        UpdatePassword: action.payload.message
      }
    case reducerType.UPDATE_PROFILE:
      return {
        ...state,
        UpdateProfile: action.payload.message,
        loginData: action.payload.status,
        patientinfo: action.payload.patientinfo,
      }
    case reducerType.CLEAR_PROFILE_DATA:
      return {
        ...state,
        UpdateProfile: null,
      }
      break

    case reducerType.GET_VIDEO_RESOURSES:
      return {
        ...state,
        videoResources: action.payload
      }
      break
    case reducerType.GET_ARTICLE_RESOURSES:
      return {
        ...state,
        articleResources: action.payload
      }
      break

      case reducerType.FORGOT_PASSWORD:
      return {
        ...state,
        ForgotPassword: action.payload.message
      }
      break
      
    default:
      return state
  }
}
export default GetModuleType;