import React, { Component } from "react";
import { Image, ImageBackground, View, Text } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

export const DrawerHeader = props => {
  return (
    <View style={styles.topVIew}>
      <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
        <Image
          style={{  marginTop: 30, marginStart:15, width: 80, height: 50 }}
          source={require("../../../asset/app_logo.png")}
        />
        <Image
          style={{ height: 70, width: 70 , borderRadius:35, marginTop:20,borderWidth:1,borderColor:'gray',marginEnd:20}}
          source={require("../../../asset/profile.jpg")}
        />
      </View>

      <View>
        <Text style={[styles.nameText, { fontWeight: "bold" }]}>
          {props.Name}
        </Text>
        <Text style={[styles.nameText, { fontSize: 16 }]}>{props.Email}</Text>
      </View>
    </View>
  );
};

const styles = {
  topVIew: {
    width: "100%",
    height: 145,
    backgroundColor: "#fff",
    marginBottom: 10
  },
  titleAlign: {
    marginStart: 20,
    marginTop: 25,
    marginBottom: 10,
    color: "#00cccc",
    fontWeight: "bold",
    fontSize: 25
  },
  nameText: {
    marginLeft: 25,
    marginTop: 5,
    color: "#00cccc",
    fontSize: 20
  },
  image: {
    width: 70,
    height: 65,
    marginLeft: 25,
    marginTop: 20,
    borderRadius: 25,
    marginBottom: 20
  }
};
