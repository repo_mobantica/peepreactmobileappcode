import { AppRegistry } from 'react-native';
import App from './App';
if (typeof process === 'undefined') process = {};
process.nextTick = setImmediate;

module.exports = process;

AppRegistry.registerComponent('reduxData', () => App);
